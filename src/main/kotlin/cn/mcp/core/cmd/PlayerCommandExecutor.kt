package cn.mcp.core.cmd

import cn.mcp.core.buildErrorChat
import org.bukkit.ChatColor
import org.bukkit.command.Command
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player

abstract class PlayerCommandExecutor : CommandExecutor() {

    override fun onCommand(
        sender: CommandSender,
        command: Command,
        label: String,
        args: Array<out String>
    ): Boolean {
        return if (sender is Player) {
            val result = onCommand(sender, command, label, args)
            if (!result) {
                sender.sendMessage(buildErrorChat(command.usage, ChatColor.RED))
            }
            true
        } else {
            sender.run {
                sendMessage(buildErrorChat("当前命令必须由玩家执行"))
//                sendMessage("The current command must be executed by the player")
            }
            false
        }
    }

    abstract fun onCommand(
        player: Player,
        command: Command,
        label: String,
        args: Array<out String>
    ): Boolean
}