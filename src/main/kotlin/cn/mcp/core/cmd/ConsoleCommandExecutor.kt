package cn.mcp.core.cmd

import cn.mcp.core.buildSystemChat
import org.bukkit.command.Command
import org.bukkit.command.CommandSender
import org.bukkit.command.ConsoleCommandSender

abstract class ConsoleCommandExecutor : CommandExecutor() {

    override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>): Boolean {
        return if (sender is ConsoleCommandSender) {
            onCommand(sender, command, label, args)
        } else {
            sender.run {
                sendMessage(buildSystemChat("当前命令只能在控制台执行"))
//                sendMessage("The current command can only be executed at the console")
            }
            false
        }
    }

    abstract fun onCommand(
        sender: ConsoleCommandSender,
        command: Command,
        label: String,
        args: Array<out String>
    ): Boolean

}