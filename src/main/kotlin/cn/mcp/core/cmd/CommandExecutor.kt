package cn.mcp.core.cmd

import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.command.TabCompleter
import java.util.*

abstract class CommandExecutor : CommandExecutor, TabCompleter {

    abstract fun getCommandName(): String

    fun getAliases(): List<String>? {
        return null
    }

    override fun onTabComplete(
        sender: CommandSender,
        command: Command,
        alias: String,
        args: Array<out String>
    ): List<String> {
        return ArrayList()
    }

}