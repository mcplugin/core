package cn.mcp.core

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

fun launch(context: CoroutineContext = Dispatchers.Default, block: suspend () -> Unit): Job {
    return CoroutineScope(context).launch { block.invoke() }
}