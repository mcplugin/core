package cn.mcp.core.ktx

import cn.mcp.core.cmd.CommandExecutor
import org.bukkit.event.Listener
import org.bukkit.plugin.java.JavaPlugin

/**
 * 注册事件
 * @param events 事件
 */
fun JavaPlugin.registerEvents(vararg events: Listener) {
    val pm = server.pluginManager
    events.forEach {
        pm.registerEvents(it, this)
    }
}

/**
 * 添加命令执行器
 * @param executors 执行器
 */
fun JavaPlugin.addCommandExecutors(vararg executors: CommandExecutor) {
    executors.forEach {
        getCommand(it.getCommandName())?.run {
            setExecutor(it)
            tabCompleter = it
            it.getAliases()?.run {
                aliases = this
            }
        }
    }

}