package cn.mcp.core.ktx

import cn.mcp.core.buildColorText
import net.md_5.bungee.api.ChatMessageType
import net.md_5.bungee.api.chat.TextComponent
import org.bukkit.Bukkit
import org.bukkit.ChatColor
import org.bukkit.entity.Player
import org.bukkit.plugin.Plugin

/**
 * 玩家隐身
 */
fun Player.invisibility(plugin: Plugin) {
    Bukkit.getOnlinePlayers().forEach {
        it.hidePlayer(plugin, this)
    }
}

/**
 * 显示/取消隐身
 */
fun Player.show(plugin: Plugin) {
    Bukkit.getOnlinePlayers().forEach {
        it.showPlayer(plugin, this)
    }
}

fun Player.sendActionbarMessage(message: String) {
    spigot().sendMessage(ChatMessageType.ACTION_BAR, TextComponent(buildColorText(message, ChatColor.WHITE)))
}