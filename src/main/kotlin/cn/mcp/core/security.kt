package cn.mcp.core

import java.security.MessageDigest

private fun toHex(byteArray: ByteArray): String {
    return with(StringBuilder()) {
        byteArray.forEach {
            val hex = it.toInt() and (0xFF)
            val toHexString = Integer.toHexString(hex)
            if (toHexString.length == 1) {
                append("0$toHexString")
            } else {
                append(toHexString)
            }
        }
        toString()
    }
}

fun String.md5(): String {
    val md = MessageDigest.getInstance("MD5")
    val digest = md.digest(this.toByteArray())
    return toHex(digest)
}
