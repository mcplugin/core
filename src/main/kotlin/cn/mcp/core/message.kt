package cn.mcp.core

import org.bukkit.ChatColor

private const val CHAT_BROADCAST = "{广播}"
private const val CHAT_SYSTEM = "{系统}"
private const val CHAT_ALL = "{全部}"
private const val CHAT_TEAM = "{组队}"
private const val CHAT_PRIVATE = "{私聊}"
private const val CHAT_ERROR = "{错误}"

fun buildChat(type: String, chat: String, vararg colors: ChatColor) = buildColorText("$type$chat", *colors)

fun buildBroadcastChat(chat: String, color: ChatColor = ChatColor.GREEN) = buildChat(CHAT_BROADCAST, chat, color)

fun buildAllChat(chat: String) = buildChat(CHAT_ALL, chat)

fun buildSystemChat(chat: String, color: ChatColor = ChatColor.YELLOW) = buildChat(CHAT_SYSTEM, chat, color)

fun buildTeamChat(chat: String, color: ChatColor = ChatColor.BLUE) = buildChat(CHAT_TEAM, chat, color)

fun buildPrivateChat(chat: String, color: ChatColor = ChatColor.GRAY) = buildChat(CHAT_PRIVATE, chat, color)

fun buildErrorChat(chat: String, color: ChatColor = ChatColor.RED) = buildChat(CHAT_ERROR, chat, color)

fun errorMessage(message: String) = buildColorText(message, ChatColor.RED)

fun hintMessage(message: String) = buildColorText(message, ChatColor.YELLOW)

fun buildColorText(message: String, vararg colors: ChatColor) = "${colors.joinToString("")}$message"